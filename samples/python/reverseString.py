import math

def reverseStringWithTemp(data):
    ''' This method doesn't destroy the value because parameters passed by value '''
    print(data)
    for i in range(0, math.floor(len(data)/2)):
        pre = data[:i]
        first_replace = data[i]
        middle = data[i+1:len(data)-(i+1)]
        last_replace = data[len(data)-(i+1)]
        post = data[len(data)-i:]
        
        data = pre + last_replace + middle + first_replace + post
        print(data)

def reverseListWithTemp(list):
    ''' Lists are mutable in python so they are passed by reference and modified within the function '''
    print(list)
    for i in range(0, math.floor(len(list)/2)):
        t = list[i]
        list[i] = list[len(list)-(i+1)]
        list[len(list)-(i+1)] = t
        print(list)


# Test reversals without using reverse slice notation [::-1]

# one , twelve , onomonopea
data = "onomonopea"
reverseStringWithTemp(data)
print("Result: " + data)

list = [1,2,3,4,5]
reverseListWithTemp(list)
print("Result: " + str(list))