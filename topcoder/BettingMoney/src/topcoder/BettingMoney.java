package topcoder;

public class BettingMoney {

	public static void main(String... args) {
		BettingMoney m = new BettingMoney();
		
		assert(m.moneyMade(new int[]{10,20,30}, new int[]{20,30,40}, 1) == 3400);
		assert(m.moneyMade(new int[]{200,300,100}, new int[]{10,10,10}, 2) == 49000);
		assert(m.moneyMade(new int[]{100,100,100,100}, new int[]{5,5,5,5}, 0) == 29500);
		assert(m.moneyMade(new int[]{5000,5000}, new int[]{100,2}, 0) == 0);
		assert(m.moneyMade(new int[]{100}, new int[10], 0) == -1000);
	}
	
	public int moneyMade(int[] amounts, int[] centsPerDollar, int finalResult) {
		return Integer.MIN_VALUE;
	}
}
